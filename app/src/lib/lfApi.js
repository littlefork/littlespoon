import store from '../../store';
import { addMessage } from '../../reduxActions/messages';

const check = (remote) => fetch(`${remote.url}/projects`,
  {
    headers: {
      apikey: remote.apiKey || '',
    }
  })
.then(({ status }) => {
  if (status > 200 || status < 200) {
    throw new Error();
  }
  return true;
})
.catch(() => false);

const get = (remote, path) =>
  fetch(`${remote.url}/${path}`,
    {
      headers: {
        apikey: remote.apiKey || '',
      }
    })
  .then(r => r.json())
  .catch(err => {
    console.error(err);
    console.error('returning empty array');
    store.dispatch(addMessage({
      text: `could not connect to remote ${remote.name} ${remote.url}`,
      details: err,
      level: 'error',
      seen: false,
    }));
    return [];
  });

const post = (remote, path, payload) =>
  fetch(`${remote.url}/${path}`,
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        apikey: remote.apiKey || '',
      },
      method: 'POST',
      body: JSON.stringify(payload)
    })
  .then(r => r.json())
  .catch((err) => {
    console.error(err);
    console.error('returning empty array');
    return [];
  });

export default { get, post, check };
