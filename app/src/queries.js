import _ from 'lodash/fp';

import api from './lib/lfApi';
import {sha256} from './lib/utils';

const queryIn = _.curry(
  (q) => _.merge(q, {
    id: q.id || sha256(q.name)
  }));

const queryOut = _.curry(
  (q) => _.merge(q, {
    id: q.id || sha256(q.name),
  }));

const queries = (project) =>
  api.get(project.remote, `projects/${project.id}/queries`)
    .then(_.map(queryIn()));

const upsert = (p, q) =>
  api.post(p.remote, `projects/${p.id}/queries`, queryOut(q));

export default {
  queries, upsert
};
