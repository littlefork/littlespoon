import React, { Component } from 'react';
import Header from './Header';

export default class App extends Component {
  props: {
    children: HTMLElement
  };

  render() {
    return (
      <div>
        <Header location={this.props.location} />
        {this.props.children}
      </div>
    );
  }
}
