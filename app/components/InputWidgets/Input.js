import React, { Component } from 'react';

import TextInput from './TextInput';
import BooleanInput from './BooleanInput';
import NumberInput from './NumberInput';

export default class Input extends Component {

  render() {
    const t = typeof this.props.value;
    switch (t) {
      case 'string':
        return (<TextInput
          name={this.props.name}
          label={this.props.label}
          change={this.props.change}
          value={this.props.value}
        />);
      case 'boolean':
        return (<BooleanInput
          name={this.props.name}
          label={this.props.label}
          change={this.props.change}
          value={this.props.value}
        />);
      case 'number':
        return (<NumberInput
          name={this.props.name}
          label={this.props.label}
          change={this.props.change}
          value={this.props.value}
        />);
      default:
        return (<TextInput
          name={this.props.name}
          label={this.props.label}
          change={this.props.change}
          value={this.props.value}
        />);
    }
  }
}

Input.propTypes = {
  name: React.PropTypes.string.isRequired,
  label: React.PropTypes.string.isRequired,
  // value: React.PropTypes.string,
  change: React.PropTypes.func.isRequired,
};

Input.defaultProps = {
  value: '',
};
