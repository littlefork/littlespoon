import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';
import _ from 'lodash/fp';

import { upsertQuery } from '../../../../reduxActions/queryActions';

import { prefilledConfig, filterQueryPlugins, validate } from '../RunEditorUtils';
import QueryEditorComponent from '../../../../components/Editors/QueryEditorComponent';
import FieldList from '../../../../components/InputWidgets/FieldList';
import TextInput from '../../../../components/InputWidgets/TextInput';

import Query from '../../../../components/InputWidgets/Query';

import ConfigEditor from '../ConfigEditor';

// map with keys
const mapW = _.map.convert({ cap: false });

function mapStateToProps(state) {
  const { queries, plugins, selectedProject } = state;
  return {
    query: queries.configureObject,
    plugins: plugins.plugins,
    project: selectedProject,
  };
}

class QueryEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: _.merge({}, this.props.query),
    };  // copy query from props as state
    this.changeInput = this.changeInput.bind(this);
    this.selectQuery = this.selectQuery.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateConfig = this.updateConfig.bind(this);
  }

  changeInput(objPath, value) {
    return this.setState(
      Object.assign(this.state, {query: _.set(objPath, value)(this.state.query)})
    );
  }

  // get the current full plugins
  activePlugins() {
    const allPlugins = this.props.plugins;
    const selected = _.map(q => q.plugin)(this.state.query.queries);
    const activePlugins = _.filter(p => selected.includes(p.name))(allPlugins);
    return activePlugins;
  }

  // update query.config in state after a new query is selected. Listens to state.query.queries and
  // creates state.query.config with defaults and all that
  updateConfig() {
    const c = _.merge(
      prefilledConfig(this.activePlugins()),
      _.get('config', this.state.query),
    );
    return this.setState(_.set('query.config', c, this.state));
  }

  // action when a query is selected from the list
  selectQuery(qPath, pluginName) {
    const argName = _.find((p) => p.name === pluginName, this.props.plugins).query_arg.name || '';
    const q = {
      plugin: pluginName,
      type: argName,
    };
    return this.setState(_.set(qPath, q)(this.state), this.updateConfig);
  }

  // submit the react state to redux store
  handleSubmit(event) {
    event.preventDefault();
    if (_.isEmpty(validate(this.state.query))) {
      this.props.dispatch(upsertQuery(this.props.project, this.state.query));
      hashHistory.goBack();
    } else {
      // scroll to error messages on top if form cannot be submitted
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { query } = this.state;

    const change = this.changeInput;
    const changeConfig = _.curry((cName, value) => change(`config.${cName}`, value));

    const queryFields =
      mapW((q, i) => {
        const plugin = _.find((p) => p.name === q.plugin)(this.props.plugins);
        return (
          <Query
            selectValue={q.plugin}
            selectChange={(e) => this.selectQuery(`query.queries[${i}]`, e.target.value)}
            options={filterQueryPlugins(this.props.plugins)}
            name={`t${i}`}
            inputValue={q.term || ''}
            label={_.get('query_arg.description', plugin) || ''}
            inputChange={(e) => change(`queries[${i}].term`, e.target.value)}
          />
        );
      }, query.queries.concat({}));

    const metaFields = [
      <TextInput
        name="name"
        value={query.name || ''}
        label="Name"
        change={(e) => change('name', e.target.value)}
      />,
      <TextInput
        name="description"
        value={query.description || ''}
        label="Description"
        change={(e) => change('description', e.target.value)}
      />,
    ];

    const cEditor = (<ConfigEditor
      plugins={this.activePlugins()}
      config={this.state.query.config}
      change={changeConfig}
    />);

    return (
      <QueryEditorComponent
        errors={validate(this.state.query)}
        query={query}
        submit={(e) => this.handleSubmit(e)}
        metaFields={<FieldList fields={metaFields} />}
        queryFields={<FieldList fields={queryFields} />}
        configFields={cEditor}
      />);
  }
}

export default connect(mapStateToProps)(QueryEditor);
