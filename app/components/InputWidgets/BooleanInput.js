import React, { Component } from 'react';

import styles from '../../sass/form.sass';

export default class TextInput extends Component {

  render() {
    return (
      <div className={`${styles.field}`}>
        <label htmlFor={this.props.name}>{this.props.label}</label>
        <input
          type="checkbox"
          name={this.props.name}
          value={this.props.value}
          onChange={this.props.change}
        />
      </div>
    );
  }
}

TextInput.propTypes = {
  name: React.PropTypes.string.isRequired,
  label: React.PropTypes.string.isRequired,
  value: React.PropTypes.bool,
  change: React.PropTypes.func.isRequired,
};

TextInput.defaultProps = {
  value: '',
};
