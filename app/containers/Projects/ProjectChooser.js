import { connect } from 'react-redux';
// import _ from 'lodash/fp';
import React, { Component } from 'react';

// Redux Actions
import { selectProject, editProject, editRemote } from '../../reduxActions/projectActions';

// React Visual Components
// import ProjectList from '../../components/Projects/ProjectList';
import Remotes from '../../components/Remotes/Remotes';
// import Project from '../../components/Projects/Project';

// Map Redux State to the react container
const mapStateToProps = (state) =>
  ({
    projects: state.projects.projects,
    remotes: state.remotes.remotes
  });

// The react container
class ProjectChooser extends Component {

  projectClicked(e, p) {
    e.preventDefault();
    this.props.router.push(`/${p.slug}`);
    this.props.dispatch(selectProject(p));
  }

  editProject(p) {
    // e.preventDefault();
    this.props.router.push('/edit');
    this.props.dispatch(editProject(p));
  }

  addProject(e) {
    e.preventDefault();
    this.props.router.push('/edit');
    this.props.dispatch(editProject({}));
  }

  newRemote() {
    this.props.router.push('/remote');
    this.props.dispatch(editRemote({}));
  }
  render() {
    return (
      <Remotes
        remotes={this.props.remotes}
        addProject={this.addProject}
        newRemote={() => this.newRemote()}
        editProject={this.editProject}
        projectClicked={this.projectClicked}
      />
    );
  }
}

export default connect(mapStateToProps)(ProjectChooser);
