import React, { Component } from 'react';
import _ from 'lodash';
import Card from '../layouts/Card';
import Cancel from '../buttons/CancelButton';


export default class PipelineEditorComponent extends Component {
  render() {
    return (
      <div>
        <h2 className="section-title">Edit Pipeline</h2>
        <Card>
          <div className="card-block">
            <h2>{this.props.pipeline.name || 'New Pipeline'}</h2>
            {_.map(this.props.errors, e =>
              <div className="error">{e}</div>
            )}

            <form onSubmit={this.props.submit}>
              {this.props.metaFields}
              <hr />
              <h3>Steps</h3>
              {this.props.pipelineFields}
              <hr />
              <h3>Configuration</h3>
              {this.props.configFields}
              <hr />
              <input type="submit" value="save" />
            </form>
            <Cancel />
          </div>
        </Card>
      </div>
    );
  }
}

//
// PipelineEditorComponent.propTypes = {
//   pipeline: React.PropTypes.object.isRequired,
//   submit: React.PropTypes.func.isRequired,
//   metaFields: React.PropTypes.array.isRequired,
//   pipelineFields: React.PropTypes.array.isRequired,
//   configFields: React.PropTypes.array.isRequired,
// };
