import { connect } from 'react-redux';
import React, { Component } from 'react';
import { hashHistory } from 'react-router';
import uuid from 'uuid';

import RemoteComponent from '../../components/Editors/RemoteEditorComponent';

import {addRemote} from '../../reduxActions/projectActions';

class AddRemote extends Component {
  constructor(props) {
    super(props);
    const r = this.props.remote;
    this.state = {
      id: !r === '' ? r.id : uuid.v4(),
      name: r.name,
      url: r.url,
      apiKey: r.apiKey,
      active: r.active || true
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const r = this.state;
    this.props.dispatch(addRemote(r));
    hashHistory.push('/');
  }

  render() {
    return (<RemoteComponent
      handleSubmit={this.handleSubmit}
      handleInputChange={this.handleInputChange}
      remote={this.state}
    />);
  }
}


function mapStateToProps(state) {
  const remote = state.remotes.configureObject;
  return {
    remote
  };
}

export default connect(mapStateToProps)(AddRemote);
