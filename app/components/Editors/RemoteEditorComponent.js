import React, { Component } from 'react';

import CancelButton from '../../components/buttons/CancelButton';
import styles from '../../sass/form.sass';

export default class RemoteEditorComponent extends Component {

  render() {
    return (
      <div>
        <div className={`${styles.heading}`}>
          <h1>
            <i className="fa fa-database" /> Add Remote
          </h1>
        </div>
        <form
          className={`${styles.tableGridWrapper} ${styles.withForm} active`}
          onSubmit={this.props.handleSubmit}
        >
          <div className={`${styles.field}`}>
            <label htmlFor="name">Name:</label>
            <input
              name="name"
              type="text"
              value={this.props.remote.name}
              onChange={this.props.handleInputChange}
              placeholder="example: My Computer"
            />
          </div>
          <div className={`${styles.field}`}>
            <label htmlFor="url">api url and port:</label>
            <textarea
              name="url"
              value={this.props.remote.url}
              onChange={this.props.handleInputChange}
              placeholder="example: https://localhost:8000"
            />
          </div>
          <div className={`${styles.field}`}>
            <label htmlFor="apiKey">api key</label>
            <input
              type="text"
              name="apiKey"
              value={this.props.remote.apiKey}
              onChange={this.props.handleInputChange}
            />
          </div>
          <div className={`${styles.field}`}>
            <label htmlFor="active">Active</label>
            <input
              name="active"
              type="checkbox"
              defaultChecked={(this.props.remote.active)}
              onChange={this.props.handleInputChange}
            />
          </div>
          <div className={`${styles.buttons}`}>
            <input
              type="submit"
              value="save"
              className="btn btn-md"
            />
            <CancelButton />
          </div>
        </form>
      </div>
    );
  }
}


RemoteEditorComponent.propTypes = {
  remote: React.PropTypes.object.isRequired,
  handleSubmit: React.PropTypes.func.isRequired,
  handleInputChange: React.PropTypes.func.isRequired,
};
