import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import Run from './Run';

class Log extends Component {

  render() {
    return (
      <div>
        <h2 className="section-title">Log</h2>
        {_.map(this.props.runs, run =>
          <Run key={run.id} run={run} />
        )}

      </div>
    );
  }
}

function mapStateToProps(state) {
  const { runs } = state;

  return {
    runs: runs.runs,
  };
}

export default connect(mapStateToProps)(Log);
