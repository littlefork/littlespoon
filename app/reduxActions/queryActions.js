
import { queries, upsert } from '../src/queries';
import { updateProject } from './projectActions';
import { addMessage } from './messages';

// QUERY ACTIONS

export const REQUEST_QUERIES = 'REQUEST_QUERIES';
export const RECEIVE_QUERIES = 'RECEIVE_QUERIES';
export const SELECT_QUERY = 'SELECT_QUERY';
export const CONFIGURE_QUERY = 'CONFIGURE_QUERY';
export const UPSERT_QUERY = 'UPSERT_QUERY';

export const requestQueries = () => ({
  type: REQUEST_QUERIES,
});

export const receiveQueries = (json) => ({
  type: RECEIVE_QUERIES,
  payload: json
});

export const fetchQueries = (project) =>
  dispatch => {
    dispatch(requestQueries);
    return queries(project)
      .then(json => dispatch(receiveQueries(json)));
  };

export const configureQuery = (action) => ({
  type: CONFIGURE_QUERY,
  action
});

export const upsertQuery = (project, query) =>
  dispatch =>
    upsert(project, query)
      .then(q => dispatch({
        type: UPSERT_QUERY,
        payload: q
      }))
      .then(() => dispatch(addMessage({
        text: `query ${query.name} successfully updated!`,
        seen: false,
        level: 'success',
      })))
      .then(() => dispatch(updateProject(project, query.config)));
