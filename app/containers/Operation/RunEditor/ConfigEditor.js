import React, { Component } from 'react';
import _ from 'lodash/fp';

import { pluginConfig } from './RunEditorUtils';
import FieldList from '../../../components/InputWidgets/FieldList';
import Input from '../../../components/InputWidgets/Input';

// takes props:
// plugins - list of plugins to get config from
// config - current config to read config from
// change - function to call when input changed.  sends config name and value back

export default class ConfigEditor extends Component {
  // Return a list of configuration options as the selected plugins supply them
  configs() {
    return pluginConfig(this.props.plugins);
  }

  render() {
    const configFields = _.map(c => (
      <Input
        name={c.name}
        value={_.get(c.name, this.props.config)}
        label={c.description}
        change={e => this.props.change(c.name, e.target.type === 'checkbox' ? e.target.checked : e.target.value)}
      />))(this.configs());

    return <div><FieldList fields={configFields} /></div>;
  }
}
