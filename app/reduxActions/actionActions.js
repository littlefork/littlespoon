export const ADD_ACTION = 'ADD_ACTION';
export const REMOVE_ACTION = 'REMOVE_ACTION';
export const REMOVE_ALL_ACTIONS = 'REMOVE_ALL_ACTIONS';
export const SET_ACTIVE_RUN = 'SET_ACTIVE_RUN';
export const REMOVE_ACTIVE_RUN = 'REMOVE_ACTIVE_RUN';

// RunEditor

export const addAction = (action) => ({
  type: ADD_ACTION,
  action
});

export const removeAction = (action) => ({
  type: REMOVE_ACTION,
  action
});

export const removeAllActions = () => ({
  type: REMOVE_ALL_ACTIONS
});

export const setActiveRun = (run) => ({
  type: SET_ACTIVE_RUN,
  run
});

export const removeActiveRun = () => ({
  type: REMOVE_ACTIVE_RUN
});
