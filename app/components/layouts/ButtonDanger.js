import React, { Component } from 'react';

export default class ButtonDanger extends Component {
  props: {
    children: HTMLElement
  };

  render() {
    return (
      <button type="button" className="btn btn-danger">
        {this.props.children}
      </button>
    );
  }
}
