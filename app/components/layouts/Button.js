import React, { Component } from 'react';

export default class Button extends Component {
  props: {
    children: HTMLElement
  };

  render() {
    return (
      <button className="btn btn-sm">
        {this.props.children}
      </button>
    );
  }
}
