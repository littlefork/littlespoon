import nn from 'node-notifier';

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const HIDE_MESSAGE = 'HIDE_MESSAGE';

export const addMessage = (message) => {
  const options = {
    title: 'Littlespoon',
    message: message.text
  };
  new nn.NotificationCenter().notify(options);
  new nn.NotifySend().notify(options);
  new nn.WindowsToaster().notify(options);
  new nn.WindowsBalloon().notify(options);
  new nn.Growl().notify(options);

  return {
    type: ADD_MESSAGE,
    message
  };
};

export const hideMessage = (message) => ({
  type: HIDE_MESSAGE,
  message
});
