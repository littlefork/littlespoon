import _ from 'lodash';

import { runs, upsert, trigger } from '../src/runs';
import { addAction } from './actionActions';
import { addMessage } from './messages';

// RUN ACTIONS

export const REQUEST_RUNS = 'REQUEST_RUNS';
export const RECEIVE_RUNS = 'RECEIVE_RUNS';

export const requestRuns = () => ({
  type: REQUEST_RUNS,
});

export const receiveRuns = (json) => ({
  type: RECEIVE_RUNS,
  payload: json
});

export const fetchRuns = (project) =>
  dispatch => {
    dispatch(requestRuns);
    return runs(project)
      .then(json => dispatch(receiveRuns(json)));
  };

export const openRunInEditor = (run) =>
  dispatch => {
    _.forEach(run.actions, a => {
      dispatch(addAction(a));
    });
  };

export const submitRun = (project, run) =>
  dispatch => {
    const r = run;
    return upsert(project, run)
      .catch((e) => dispatch(addMessage({
        text: `something went wrong with run ${run.id}, ${e}`,
        level: 'error',
      })))
      .then((rr) => { r.id = rr.id; return true; })
      .then(() => dispatch(addMessage({
        text: `Run ${r.id} saved`,
        level: 'success',
      })))
      .then(() => trigger(project, r))
      .catch((e) => dispatch(addMessage({
        text: `something went wrong with run ${r.id}, ${e}`,
        level: 'error',
      })))
      .then(() => dispatch(addMessage({
        text: `Run ${r.id} running!`,
        level: 'success',
      })))
      .then(() => dispatch(fetchRuns(project)));
  };
