

import { pipelines, upsert } from '../src/pipelines';
import { updateProject } from './projectActions';
import { addMessage } from './messages';

// PIPELINE ACTIONS

export const REQUEST_PIPELINES = 'REQUEST_PIPELINES';
export const RECEIVE_PIPELINES = 'RECEIVE_PIPELINES';
export const CONFIGURE_PIPELINE = 'CONFIGURE_PIPELINE';
export const UPSERT_PIPELINE = 'UPSERT_PIPELINE';

export const requestPipelines = () => ({
  type: REQUEST_PIPELINES,
});

export const receivePipelines = (json) => ({
  type: RECEIVE_PIPELINES,
  payload: json
});

export const fetchPipelines = (project) =>
  dispatch => {
    dispatch(requestPipelines);
    return pipelines(project)
      .then(json => dispatch(receivePipelines(json)));
  };

export const configurePipeline = (action) => ({
  type: CONFIGURE_PIPELINE,
  action
});

export const upsertPipeline = (project, pipeline) =>
  dispatch =>
    upsert(project, pipeline)
      .then(p => dispatch({
        type: UPSERT_PIPELINE,
        payload: p
      }))
      .then(() => dispatch(addMessage({
        text: `Pipeline ${pipeline.name} successfully updated!`,
        level: 'success',
      })))
      .then(() => dispatch(updateProject(project, pipeline.config)));
