import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { configureQuery } from '../../../../reduxActions/queryActions';

import CancelButton from '../../../../components/buttons/CancelButton';
import Card from '../../../../components/layouts/Card';

import Query from './Query';

class Queries extends Component {

  render() {
    return (
      <div className="wrapper">
        <h2 className="section-title">Queries</h2>
        <Card>
          <div className="card-block">
            {_.map(this.props.queries, q => <Query key={q.id} query={q} />)}
            <CancelButton />
            <button
              onClick={() => {
                this.props.dispatch(configureQuery({}));
                this.props.router.push(`/${this.props.selectedProject.slug}/queries/add`);
              }}
            >
              Add
            </button>
          </div>
        </Card>

      </div>
    );
  }
}

function mapStateToProps(state) {
  const queries = state.queries;
  const selectedProject = state.selectedProject;

  return {
    queries: queries.queries,
    selectedProject
  };
}

export default connect(mapStateToProps)(Queries);
