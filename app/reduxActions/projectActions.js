import _ from 'lodash';
import { projects, upsert, upsertRemote, remotes } from '../src/projects';
import { fetchQueries } from './queryActions';
import { fetchPlugins } from './plugins';
import { fetchPipelines } from './pipelineActions';
import { fetchRuns } from './runActions';
import { addMessage } from './messages';

// PROJECT ACTIONS

export const REQUEST_PROJECTS = 'REQUEST_PROJECTS';
export const UPDATE_PROJECT = 'UPDATE_PROJECT';
export const UPDATE_PROJECTS = 'UPDATE_PROJECTS';
export const SELECT_PROJECT = 'SELECT_PROJECT';
export const EDIT_PROJECT = 'EDIT_PROJECT';
export const ADD_REMOTE = 'ADD_REMOTE';
export const EDIT_REMOTE = 'EDIT_REMOTE';
export const UPDATE_REMOTES = 'UPDATE_REMOTES';
export const CHECK_REMOTE = 'CHECK_REMOTE';
export const UPSERT_PROJECT = 'UPSERT_PROJECT';

export const requestProjects = () => ({
  type: REQUEST_PROJECTS,
});

export const updateProjects = (json) => ({
  type: UPDATE_PROJECTS,
  payload: json
});

export const updateProject = (project, config) => ({
  type: UPDATE_PROJECT,
  config,
  project
});

export const selectProject = (project) =>
  dispatch => {
    dispatch({
      type: SELECT_PROJECT,
      payload: project
    });
    dispatch(fetchQueries(project));
    dispatch(fetchRuns(project));
    dispatch(fetchPipelines(project));
    dispatch(fetchPlugins(project));
  };

export const editProject = (project) => ({
  type: EDIT_PROJECT,
  payload: project
});

export const fetchProjects = () =>
  dispatch => {
    dispatch(requestProjects);
    return projects()
      .then(json => dispatch(updateProjects(json)));
  };

export const upsertProject = (project) =>
  dispatch =>
    upsert(project)
      .catch(() => console.log('whoooops'))
      .then(p => dispatch({
        type: UPSERT_PROJECT,
        payload: p
      }))
      .then(() => dispatch(addMessage({
        text: `project ${project.name} successfully updated!`,
        seen: false,
        level: 'success',
      })));


export const fetchRemotes = () =>
  dispatch =>
    remotes()
      .then(json => dispatch({
        type: UPDATE_REMOTES,
        payload: _.without(json, 'remote')
      })).then(() => dispatch(fetchProjects()));

export const addRemote = (remote) =>
  dispatch =>
    upsertRemote(remote)
      .then(() => dispatch({
        type: 'ADD REMOTE',
        payload: []
      }))
      .then(() => dispatch(fetchRemotes()));

export const editRemote = (remote) => ({
  type: EDIT_REMOTE,
  payload: remote
});
