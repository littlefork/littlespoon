import React, { Component } from 'react';
import { hashHistory } from 'react-router';
// import Button from '../layouts/Button';
import styles from '../../sass/form.sass';

// going back doesnt work yet!
export default class ChancelButton extends Component {

  render() {
    return (
      <button
        className={`${styles.cancelButton}`}
        onClick={() => hashHistory.goBack()}
      >
        <i className="fa fa-times" /> Cancel
      </button>
    );
  }
}
