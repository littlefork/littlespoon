import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';

import Card from '../../../../components/layouts/Card';
import styles from '../../../../sass/QueryAction.sass';

import { removeAction } from '../../../../reduxActions/actionActions';
import { configureQuery } from '../../../../reduxActions/queryActions';

class QueryAction extends Component {

  render() {
    return (
      <Card>
        <div className={styles.heading}>
          <h3 className="white">Get</h3>
          <span className="conf">
            <Link onClick={() => this.props.dispatch(configureQuery(this.props.action))}
                  to={`/${this.props.selectedProject.slug}/queries/add`}
            >
              Configure
            </Link>
          </span>
          <span className="remove">
            <Link
              onClick={() => this.props.dispatch(removeAction(this.props.action))}
              to={`/${this.props.selectedProject.slug}`}
            >
              Remove
            </Link>
          </span>
        </div>
        <div className="card-block">
          <h4>{this.props.action.name}</h4>
        </div>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  const { selectedProject, queries } = state;
  return {
    selectedProject,
    queries
  };
}

export default connect(mapStateToProps)(QueryAction);
