import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';


import Header from '../ProjectHeader';
import Log from './Log/Log';

class Operations extends Component {
  render() {
    if (!this.props.selectedProject.id) {
      hashHistory.push('/');
    }
    return (
      <div>
        <Header
          projectName={this.props.selectedProject.name}
        />

        <div className="wrapper">
          {
            // runeditor or other things go here from routes
            this.props.children
          }
          <Log runs={this.props.runs} />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { selectedProject, runs } = state;

  return {
    selectedProject,
    runs,
  };
}

export default connect(mapStateToProps)(Operations);
