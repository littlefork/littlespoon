import { connect } from 'react-redux';
import React, { Component } from 'react';
import { hashHistory } from 'react-router';
import slug from 'slug';
import _ from 'lodash';

import {upsertProject} from '../../reduxActions/projectActions';

import ProjectEditorComponent from '../../components/Editors/ProjectEditorComponent';
import FieldList from '../../components/InputWidgets/FieldList';
import TextInput from '../../components/InputWidgets/TextInput';
import Select from '../../components/InputWidgets/Select';

const mapStateToProps = (state) => {
  const project = state.projects.configureObject;

  return {
    project,
    remotes: state.remotes.remotes,
  };
};

class EditProject extends Component {
  constructor(props) {
    super(props);
    const p = this.props.project;
    this.state = {
      name: p.name,
      desc: p.desc,
      remote: p.remote || this.props.remotes[0],
      config: p.config || {},
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleConfigChange = this.handleConfigChange.bind(this);
    this.handleRemoteChange = this.handleRemoteChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleRemoteChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    this.setState({
      remote: _.find(this.props.remotes, x => x.id === value)
    });
  }

  handleConfigChange(e) {
    this.setState(_.merge({}, this.state,
      {
        config: {
          [e.target.name]: e.target.value
        }
      })
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    const newP = _.merge(this.props.project, this.state, {
      slug: this.props.project.slug || slug(this.state.name)
    });
    this.props.dispatch(upsertProject(newP));
    hashHistory.push('/');
  }

  render() {
    const metaFields = [
      <TextInput
        name="name"
        value={this.state.name}
        change={(e) => this.handleInputChange(e)}
        label="Name"
      />,
      <TextInput
        name="desc"
        value={this.state.desc}
        change={this.handleInputChange}
        label="Description"
      />,
      <Select
        label="remote"
        name="remote"
        value={this.state.remote.id}
        change={this.handleRemoteChange}
        options={_.map(this.props.remotes, (r, i) =>
          <option key={i} value={r.id}>{r.name}</option>
        )}
      />
    ];

    const configFields = _.map(this.props.project.config, (v, k) =>
      <TextInput
        name={k}
        value={this.state.config[k]}
        change={this.handleConfigChange}
        label={k}
        key={k}
      />,
    );
    return (<ProjectEditorComponent
      project={this.props.project}
      submit={this.handleSubmit}
      metaFields={<FieldList fields={metaFields} />}
      configFields={<FieldList fields={configFields} />}
    />);
  }
}

export default connect(mapStateToProps)(EditProject);
