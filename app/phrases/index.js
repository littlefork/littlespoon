const remotesPhrases = {
  pageTitle: 'Remotes',
  iconPage: 'database',
  iconHelp: 'info-circle',
  buttonHelp: 'Help',
  pageHelp: 'A remote is a URL at which a littlefork API is started and running. Littlespoon is designed to run a server on your computer when you install Littlespoon for the firtst time.',
  buttonAddNew: 'Add Remote',
  activeSectionTitle: 'Available Remotes',
  activeSectionHelp: 'Active remotes are remote API\'s that are currently running and available to littlespoon',
  inactiveSectionTitle: 'Unreachable Remotes',
  inactiveSectionHelp: 'The remotes listed here either do not exist or are currently not running. Please make sure the URL and API keys are entered correctly and that they are running',
  disabledRemotesTitle: 'Disabled Remotes',
  disabledRemotesHelp: 'Disabled remotes will not be accessed',
};
const projectPhrases = {
  noProjectsMessage: 'No projects found on this remote',
  buttonAddNew: 'Add Project',
};

export default {
  remotesPhrases, projectPhrases,
};
