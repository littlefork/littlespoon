import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash/fp';
import { hashHistory } from 'react-router';

import { upsertPipeline } from '../../../../reduxActions/pipelineActions';

import { filterPipelinePlugins, validate, prefilledConfig } from '../RunEditorUtils';
import PipelineEditorComponent from '../../../../components/Editors/PipelineEditorComponent';
import FieldList from '../../../../components/InputWidgets/FieldList';
import TextInput from '../../../../components/InputWidgets/TextInput';
import Pipeline from '../../../../components/InputWidgets/Pipeline';

import ConfigEditor from '../ConfigEditor';

// map with keys
const mapW = _.map.convert({ cap: false });

function mapStateToProps(state) {
  const { pipelines, plugins, selectedProject } = state;
  return {
    selectedProject,
    meta: pipelines.configureObject,
    plugins: plugins.plugins,
    project: selectedProject,
  };
}

class PipelineEditor extends Component {
  constructor(props) {
    super(props);
    const meta = this.props.meta;
    this.state = {
      pipeline: _.merge({}, meta),
    };  // copy pipeline from props as state
    this.changeInput = this.changeInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateConfig = this.updateConfig.bind(this);
    this.activePlugins = this.activePlugins.bind(this);
  }

  changeInput(objPath, value) {
    return this.setState(
      Object.assign(this.state, {pipeline: _.set(objPath, value)(this.state.pipeline)})
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    // submit if validation does not return errors
    if (_.isEmpty(validate(this.state.pipeline))) {
      this.props.dispatch(upsertPipeline(this.props.project, this.state.pipeline));
      hashHistory.goBack();
    } else {
      // scroll to error messages on top if form cannot be submitted
      window.scrollTo(0, 0);
    }
  }

  activePlugins() {
    const allPlugins = this.props.plugins;
    const selected = this.state.pipeline.plugins;
    const activePlugins = _.filter(p => selected.includes(p.name))(allPlugins);
    return activePlugins;
  }

  updateConfig() {
    const c = _.merge(
      prefilledConfig(this.activePlugins()),
      _.get('config', this.state.pipeline),
    );
    return this.setState(_.set('pipeline.config', c, this.state));
  }

  // action when a pipeline is selected from the list
  selectPipeline(path, pluginName) {
    return this.setState(_.set(path, pluginName, this.state), this.updateConfig);
  }

  removeSelect(plugin) {
    return this.setState(
      _.set('pipeline.plugins', _.pull(plugin, this.state.pipeline.plugins), this.state));
  }

  render() {
    const { pipeline } = this.state;
    const change = this.changeInput;
    const changeConfig = _.curry((cName, value) => change(`config.${cName}`, value));

    const metaFields = [
      <TextInput
        name="name"
        value={pipeline.name}
        label="Name"
        change={(e) => change('name', e.target.value)}
      />,
      <TextInput
        name="description"
        value={pipeline.description}
        label="Description"
        change={(e) => change('description', e.target.value)}
      />
    ];

    const pipelineFields =
      mapW((pp, i) =>
        <Pipeline
          selectValue={pp}
          selectChange={(e) => this.selectPipeline(`pipeline.plugins[${i}]`, e.target.value)}
          remove={(e) => {
            e.preventDefault();
            this.removeSelect(pp);
          }}
          options={filterPipelinePlugins(this.props.plugins)}
        />
      )(pipeline.plugins.concat(""));

    const cEditor = (<ConfigEditor
      plugins={this.activePlugins()}
      config={this.state.pipeline.config}
      change={changeConfig}
    />);


    return (
      <PipelineEditorComponent
        errors={validate(this.state.pipeline)}
        pipeline={pipeline}
        plugins={filterPipelinePlugins(this.props.plugins)}
        submit={(e) => this.handleSubmit(e)}
        pipelineFields={<FieldList fields={pipelineFields} />}
        metaFields={<FieldList fields={metaFields} />}
        configFields={cEditor}
        project={this.props.project}
      />);
  }
}

export default connect(mapStateToProps)(PipelineEditor);
