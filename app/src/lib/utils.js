import _ from 'lodash';
import Promise from 'bluebird';
import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';

import {createHash} from 'crypto';

Promise.promisifyAll(fs);
Promise.promisifyAll(mkdirp);

export const sha256 = data => {
  const text = _.isString(data) ? data : JSON.stringify(data);
  return createHash('sha256').update(text).digest('hex');
};

export const readJson = (p, f) => JSON.parse(fs.readFileSync(path.resolve(p, f)));

export const readDir = p => mkdirp.mkdirpAsync(p).then(() => fs.readdirAsync(p));

export const writeJson = (p, d) => fs.writeFileAsync(p, d, 'utf8');

export const prettyJson = j => JSON.stringify(j, null, 2);

export default {
  sha256,
};
