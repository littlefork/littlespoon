import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip';
import styles from '../../sass/button.sass';

export default class Help extends Component {
  render() {
    return (
      <div className="help">
        <button
          data-tip
          data-for={this.props.dataFor}
          data-class="helpTooltip"
          data-type="light"
          data-place={this.props.place}
          data-delay-hide="600"
          data-effect="solid"
          className={`${styles.button} ${styles.help} ${this.props.buttonClass} btn btn-sm`}
        >
          <i className="fa fa-question-circle" />
        </button>
        <ReactTooltip
          id={this.props.dataFor}
          aria-haspopup="true"
          wrapper="span"
        >{this.props.helpText}</ReactTooltip>
      </div>
    );
  }
}

Help.propTypes = {
  helpText: React.PropTypes.string.isRequired,
  dataFor: React.PropTypes.string.isRequired,
  buttonClass: React.PropTypes.string,
  place: React.PropTypes.string.isRequired
};

Help.defaultProps = {
  buttonClass: '',
};
