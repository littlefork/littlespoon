import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory, browserHistory, Link } from 'react-router';

import Card from '../../../../components/layouts/Card';

import { addAction } from '../../../../reduxActions/actionActions';
import { configureQuery } from '../../../../reduxActions/queryActions';

class Query extends Component {
  render() {
    if (!this.props.selectedProject.id) {
      hashHistory.push('/');
    }
    return (
      <Card>
        <div className="card-block">
          <h2>{this.props.query.name}</h2>
          <p>{this.props.query.description}</p>
          <button
            onClick={() => {
              this.props.dispatch(addAction(this.props.query));
              browserHistory.goBack();
            }}
          >
            Add to run
          </button>
          <Link
            onClick={() => this.props.dispatch(configureQuery(this.props.query))}
            to={`/${this.props.selectedProject.slug}/queries/add`}
          >
            <button>
              Configure
            </button>
          </Link>
        </div>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  const { selectedProject, pipelines } = state;
  return {
    selectedProject,
    pipelines
  };
}

export default connect(mapStateToProps)(Query);
