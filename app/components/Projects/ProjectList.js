import React, { Component } from 'react';

export default class ProjectList extends Component {

  render() {
    return (
      <div>
        {this.props.remotes}

        <hr />
        <a
          href="#"
          onClick={this.props.addProjectClickHandler}
        >
          Add project
        </a>
        <a
          href="#"
          onClick={this.props.addRemoteClickHandler}
        >
          Add Remote
        </a>
      </div>
    );
  }
}


ProjectList.propTypes = {
  remotes: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
  addProjectClickHandler: React.PropTypes.func.isRequired,
  addRemoteClickHandler: React.PropTypes.func.isRequired,
};
