import _ from 'lodash';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import RunEditorComponent from '../../../components/Editors/RunEditorComponent';

import { removeActiveRun, removeAllActions } from '../../../reduxActions/actionActions';
import { submitRun } from '../../../reduxActions/runActions';
import { addMessage } from '../../../reduxActions/messages';

function mapStateToProps(state) {
  const { selectedProject, runEditor, runs } = state;
  return {
    selectedProject,
    runEditor,
    runs: runs.runs,
    actions: runEditor.activeRun.actions,
  };
}


class RunEditor extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.clear = this.clear.bind(this);
  }

  clear() {
    this.props.dispatch(removeActiveRun());
    this.props.dispatch(removeAllActions());
  }

  handleSubmit(event) {
    event.preventDefault();
    const project = this.props.selectedProject;
    const run = {actions: this.props.runEditor.activeRun.actions};

    if (_.isEmpty(run.actions)) {
      this.props.dispatch(addMessage({
        text: 'You cannot submit a run without actions. Please add actions to this run and resubmit.',
        level: 'error',
      }));
    } else {
      this.props.dispatch(submitRun(project, run));
    }
  }

  render() {
    return (
      <RunEditorComponent
        getSomething={(e) => {
          e.preventDefault();
          this.props.router.push(`/${this.props.selectedProject.slug}/queries`);
        }}
        doSomething={(e) => {
          e.preventDefault();
          this.props.router.push(`/${this.props.selectedProject.slug}/pipelines`);
        }}
        actions={this.props.actions}
        clear={this.clear}
        submit={(e) => this.handleSubmit(e)}
      />);
  }
}

export default connect(mapStateToProps)(RunEditor);
