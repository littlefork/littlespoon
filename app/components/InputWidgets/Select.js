import React, { Component } from 'react';

export default class Select extends Component {

  render() {
    return (
      <div>
        <label htmlFor={this}>{this.props.label}</label>
        <select
          name={this.props.name}
          value={this.props.value || 'default'}
          onChange={
            this.props.change
          }
        >
          <option disabled value="default"> -- select -- </option>
          {this.props.options}
        </select>
      </div>
    );
  }
}

Select.propTypes = {
  options: React.PropTypes.array.isRequired,
  value: React.PropTypes.string,
  change: React.PropTypes.func.isRequired,
  label: React.PropTypes.string,
  name: React.PropTypes.string,
};
