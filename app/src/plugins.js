import api from './lib/lfApi';

const plugins = (project) =>
  api.get(project.remote, 'plugins');

export default {
  plugins,
};
