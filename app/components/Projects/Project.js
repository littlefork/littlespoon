import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';
import { selectProject, editProject } from '../../reduxActions/projectActions';
// import Card from '../layouts/Card';
import styles from '../../sass/remotes.sass';

const mapStateToProps = (state) =>
({
  remotes: state.remotes.remotes
});

class Project extends Component {

  constructor(props) {
    super(props);
    this.projectClicked = this.projectSelect.bind(this);
    this.projectEdit = this.projectEdit.bind(this);
  }


  projectSelect = (e) => {
    const p = this.props.project;
    e.preventDefault();
    hashHistory.push(`/${p.slug}`);
    this.props.dispatch(selectProject(p));
  }

  projectEdit = (e) => {
    const p = this.props.project;
    e.preventDefault();
    hashHistory.push('/edit');
    this.props.dispatch(editProject(p));
  }


  render() {
    return (
      <div className={`${styles.project}`} key={this.props.project.name}>
        <div className={`${styles.slug}`}>
          <a
            href="#"
            onClick={this.projectSelect}
          >
            <i className="fa fa-folder-open" />
            {this.props.project.name}
          </a>
        </div>
        <div className={`${styles.info}`}>
          {this.props.project.desc}
        </div>
        <div className={`${styles.actions}`}>
          <button
            className="btn btn-sm"
            onClick={this.projectEdit}
          >
            <i className="fa fa-cogs" />
          </button>
        </div>
      </div>
    );
  }
}


Project.propTypes = {
  project: React.PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(Project);
