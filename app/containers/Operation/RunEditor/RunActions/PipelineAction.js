import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';

import Card from '../../../../components/layouts/Card';
import styles from '../../../../sass/PipelineAction.sass';

import { removeAction } from '../../../../reduxActions/actionActions';
import { configurePipeline } from '../../../../reduxActions/pipelineActions';

class PipelineAction extends Component {
  render() {
    return (
      <Card>
        <div className={styles.heading}>
          <h3 className="white">Do</h3>
          <span className="conf">
            <Link
              onClick={() => this.props.dispatch(configurePipeline(this.props.action))}
              to={`/${this.props.selectedProject.slug}/pipelines/add`}
            >
              Configure
            </Link>
          </span>
          <span className="remove">
            <Link
              onClick={() => this.props.dispatch(removeAction(this.props.action))}
              to={`/${this.props.selectedProject.slug}`}
            >
              Remove
            </Link>
          </span>
        </div>
        <div className="card-block">
          <h4>{this.props.action.name}</h4>
        </div>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  const { selectedProject, pipelines } = state;
  return {
    selectedProject,
    pipelines
  };
}

export default connect(mapStateToProps)(PipelineAction);
