import _ from 'lodash';

import React, { Component } from 'react';
import { Link } from 'react-router';
import Card from '../../../../components/layouts/Card';
import styles from '../../../../sass/PipelineAction.sass';

import { removeAction } from '../../../../reduxActions/actionActions';


export default class Plugin extends Component {
  render() {
    return (
      <Card>
        <div className={styles.heading}>
          <h3 className="white">Do</h3>
          <span className="remove">
            <Link
              onClick={() => this.props.dispatch(removeAction(this.props.action))}
              to={`/${this.props.project.slug}/pipelines/add`}
            >
              Remove
            </Link>
          </span>
        </div>
        <div className="card-block">
          {this.props.name}
          {this.props.fields}
        </div>
      </Card>
    );
  }
}
