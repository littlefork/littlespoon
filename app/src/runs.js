import _ from 'lodash/fp';

import api from './lib/lfApi';
import { sha256 } from './lib/utils';

import store from '../store';

const log = _.curry((l) => _.merge(l, {
  actions: _.map(a => ({id: a}))(l.actions),
}));

const runs = (project) =>
  api.get(project.remote, `projects/${project.id}/logs`)
    .then(_.map(log()));

// convert UI run object into the api format
const runOut = _.curry(
  (r) => {
    const s = store.getState();
    // const a = {
    //   id: r.uuid || sha256(r),
    //   actions: r.actions,
    //   queries: _.flow([
    //     _.filter(q => r.actions.includes(q.id)),
    //     _.flatMap(q => q.queries)
    //   ])(s.queries.queries),
    //   config: _.flow([
    //     _.filter(i => r.actions.includes(i.id)),
    //     _.map(i => i.config),
    //     _.reduce((cc, i) => _.merge(cc, i), {}),
    //   ])(_.merge(s.queries.queries, s.pipelines.pipelines)),
    // };

    // TODO: seriouslty refactor this
    const a = {};
    const all = _.concat(s.queries.queries, s.pipelines.pipelines);
    const actions = _.map(o => _.find(x => x.id === o)(all))(r.actions);

    // TODO: figure out if r uses id or uuid
    a.id = r.uuid || r.id || sha256(r);
    a.actions = r.actions;
    a.queries = _.remove(_.isNil)(_.flatMap(q => q.queries)(actions));
    a.config = _.flow([_.map(i => i.config), _.reduce((cc, i) => _.merge(cc, i), {})])(actions);

    a.config.plugins = _.remove(_.isNil)(_.flatMap(o =>
      _.concat(_.flatMap(q => q.plugin)(o.queries), o.plugins))(actions));
    console.log(a);
    return a;
  });

const upsert = (p, run) => api.post(p.remote, `projects/${p.id}/runs`, runOut(run));

const trigger = (p, r) => api.post(p.remote, `projects/${p.id}/runs/${r.id}`, {});

export default {
  runs, upsert, trigger
};
