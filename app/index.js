import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import routes from './routes';

import store from './store';

import './sass/app.global.sass';


import { fetchProjects, fetchRemotes } from './reduxActions/projectActions';
import { addMessage } from './reduxActions/messages';

// config initial store
store.dispatch(fetchProjects());
store.dispatch(fetchRemotes());
store.dispatch(addMessage({
  text: 'welcome to littlespoon',
  level: 'info',
}));

const history = syncHistoryWithStore(hashHistory, store);

render(
  <Provider store={store}>
    <div>
      <Router history={history} routes={routes} />
    </div>
  </Provider>,
  document.getElementById('root')
);
