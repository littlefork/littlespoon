technical plan in the _docs_
https://gitlab.com/littlefork/littlespoon-docs/tree/master
## RUN THE DEV SERVER

### TESTING API DATA

```
npm run testapi
```

then

```
npm run dev
```

## Principles

Containers contain NO JSX (besides root elements), no html, no css.

They outsource this to components.

Containers direct the flow and functionality of the application.  Components show just the view.

## interaction lifecycle

```
user action -> redux action -> data change -> API (POST|GET) -> message -> redux reducer -> store -> react ui change
```

stick to this!

## VM image for the API

Build requirements:

- a linux
- `git`
- `npm`
- `sudo`

The UI requires an API and a MongoDB to talk to. This can be provided manually
or with a virtual machine image. The VM is based
on [Alpine Linux](https://www.alpinelinux.org/) and the build script can be
found in `bin/build-vm`.

Build a new VM with `npm run vm:build`.

This builds a new image in `vm/tmp.XXXXXX` that can be started
using [`systemd-nspawn`](https://wiki.archlinux.org/index.php/Systemd-nspawn).
The image provides the
[littlefork-api](https://gitlab.com/littlefork/littlefork-api), a MongoDB
database and any requirements that plugins might have. Data persists between
builds of virtual machines.

Given that `systemd-nspawn` is available, start the vm: `npm run vm:start`.

To remove old images run `npm run vm:clean`. This WON'T remove any data.

In the future, this will be turned into a VirtualBox image, to make it cross
platform compatible.
