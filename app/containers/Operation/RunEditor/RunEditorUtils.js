import React from 'react';
import _ from 'lodash/fp';
import store from '../../../store';

// Get query or pipeline by id.
export const getActionById = (actions, id) =>
  _.find(a => a.id === id)(actions);

// reduce plugins to their config args
// [string] => [objects]
export const pluginConfig = (plugins) => _.uniqBy('name')(_.reduce((a, p) => a.concat(p.config_args), [])(plugins));

/* Filter plugin objects and return only Do / Pipeline -plugins where the
 * the query_args are empty by convention.
 * Argument: a list of plugin objects.
 */
export const filterPipelinePlugins = (allPlugins) =>
  _.filter(p => _.isEmpty(p.query_arg))(allPlugins);

/* Filter plugin objects and return only Get / Query -plugins where the
 * the query_args must be set by convention.
 * Argument: a list of plugin objects.
 */
export const filterQueryPlugins = (allPlugins) =>
  _.filter(p => !(_.isEmpty(p.query_arg)))(allPlugins);

export const getConfigPrefill = (path) => {
  // get current state
  const s = store.getState();
  // get configs from current state
  const l = _.map(_.get('config'), _.concat(s.queries.queries, s.pipelines.pipelines));
  // turn all configs into one config
  const u = _.reduce((c, a) => _.merge(a, c), {})(l);
  // get value from merged configs
  return _.get(path, u);
};

export const prefilledConfig = (plugins) => {
  const cs = _.map(c =>
    _.set(c.name, getConfigPrefill(c.name) || c.default, {}), pluginConfig(plugins));
  return _.mergeAll(cs);
};

// validate form by checking if name and config data are set
export const validate = (action) => {
  let errors = [];

  // check for empty values in config
  _.forIn(action.config, (v, k) => {
    if (_.isEmpty(_.get(k, action.config))) {
      errors = _.concat(errors, `Field ${k} cannot be empty!`);
    }
  });

  // check if name is not set
  if (_.isEmpty(_.get('name', action))) {
    errors = _.concat(errors, 'Name cannot be empty');
  }
  return errors;
};
