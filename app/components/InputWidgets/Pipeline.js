import React, { Component } from 'react';
import _ from 'lodash';
import Select from './Select';

export default class Pipeline extends Component {

  render() {
    return (
      <div>
        <Select
          value={this.props.selectValue}
          change={this.props.selectChange}
          options={_.map(this.props.options, p =>
            <option key={p.name} value={p.name}>{`[${p.name}] ${p.description}`}</option>)}
        />
        <a
          href="#"
          onClick={this.props.remove}
        >
          Remove
        </a>
      </div>
    );
  }
}

Pipeline.propTypes = {
  selectValue: React.PropTypes.string.isRequired,
  selectChange: React.PropTypes.func.isRequired,
  remove: React.PropTypes.func.isRequired,
  options: React.PropTypes.array.isRequired
};
