import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory, hashHistory, Link } from 'react-router';

import Card from '../../../../components/layouts/Card';

import { addAction } from '../../../../reduxActions/actionActions';
import { configurePipeline } from '../../../../reduxActions/pipelineActions';

class PipelineAction extends Component {
  render() {
    if (!this.props.selectedProject.id) {
      hashHistory.push('/');
    }
    return (
      <Card>
        <div className="card-block">
          <h2>{this.props.pipeline.name}</h2>
          <p>{this.props.pipeline.description}</p>
          <button
            onClick={() => {
              this.props.dispatch(addAction(this.props.pipeline));
              browserHistory.goBack();
            }}
          >
            Add to run
          </button>
          <Link
            onClick={() => this.props.dispatch(configurePipeline(this.props.pipeline))}
            to={`/${this.props.selectedProject.slug}/pipelines/add`}
          >
            <button>
                Configure
            </button>
          </Link>
        </div>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  const { selectedProject, pipelines } = state;
  return {
    selectedProject,
    pipelines
  };
}

export default connect(mapStateToProps)(PipelineAction);
