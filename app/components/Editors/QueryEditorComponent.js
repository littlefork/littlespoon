import React, { Component } from 'react';
import _ from 'lodash';

import Card from '../layouts/Card';
import Cancel from '../buttons/CancelButton';

export default class QueryEditorComponent extends Component {

  render() {
    return (
      <div>
        <h2 className="section-title">Edit Query</h2>
        <Card>
          <div className="card-block">
            <h2>{this.props.query.name || 'New Query'}</h2>
            {_.map(this.props.errors, (e, i) =>
              <div key={i} className="error">{e}</div>
            )}
            <form onSubmit={this.props.submit}>
              {this.props.metaFields}
              <hr />
              <h3>Queries</h3>
              {this.props.queryFields}
              <hr />
              <h3>Configuration</h3>
              {this.props.configFields}
              <hr />
              <input type="submit" value="save" />
            </form>
            <Cancel />
          </div>
        </Card>
      </div>
    );
  }
}


QueryEditorComponent.propTypes = {
  query: React.PropTypes.object.isRequired,
  submit: React.PropTypes.func.isRequired,
  metaFields: React.PropTypes.object.isRequired,
  queryFields: React.PropTypes.object.isRequired,
  configFields: React.PropTypes.object.isRequired,
};
