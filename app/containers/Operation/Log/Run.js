import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get, concat, map, filter} from 'lodash/fp';

import Card from '../../../components/layouts/Card';
import styles from '../../../sass/Run.sass';
import { openRunInEditor } from '../../../reduxActions/runActions';
import { removeAllActions } from '../../../reduxActions/actionActions';

// map with keys
const mapW = map.convert({ cap: false });

function mapStateToProps(state) {
  const { selectedProject, pipelines } = state;
  return {
    selectedProject,
    pipelines: pipelines.pipelines,
    queries: state.queries.queries,
  };
}

const fd = (iso) => {
  const d = new Date(iso);
  return (d.toLocaleString('en-GB'));
};

class Run extends Component {
  render() {
    // get the names of the actions in the log item out of the current store
    // join them into one name for the log
    const all = concat(this.props.queries, this.props.pipelines);
    const al = map(o => o.id)(this.props.run.actions);
    const as = filter(o => al.includes(o.id))(all);
    const name = map(o => `[${get('name', o)}]`)(as).join(' then ');

    return (
      <Card>
        <h3 className={styles.cardtitle}>{this.props.run.name}</h3>
        <div className="card-block">
          <b>{name}</b>
          <p>{fd(get('date', this.props.run))}</p>


          <p>id: {this.props.run.id}</p>
          <p>Status: {mapW((m, i) =>
            <li key={i}>{m.msg}</li>
            , this.props.run.stream) }</p>
        </div>
        <button
          onClick={() => {
            this.props.dispatch(removeAllActions());
            this.props.dispatch(openRunInEditor(this.props.run));
          }}
          to={`/${this.props.selectedProject.slug}/`}
        >
          Open in run editor
        </button>
      </Card>
    );
  }
}

export default connect(mapStateToProps)(Run);
