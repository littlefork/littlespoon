import React, { Component } from 'react';
import _ from 'lodash';

export default class FieldList extends Component {

  render() {
    return (
      <div>
        {_.map(this.props.fields, (f, i) =>
          <div key={i} >
            {f}
          </div>
        )}
      </div>
    );
  }
}

FieldList.propTypes = {
  fields: React.PropTypes.array.isRequired,
};
