import React, { Component } from 'react';
import _ from 'lodash';

import Select from './Select';
import TextInput from './TextInput';


export default class Query extends Component {

  render() {
    return (
      <div>
        <Select
          value={this.props.selectValue}
          change={this.props.selectChange}
          label="Query"
          options={_.map(this.props.options, p =>
            <option key={p.name} value={p.name}>{`[${p.name}] ${p.description}`}</option>)}
        />
        <TextInput
          name={this.props.name}
          value={this.props.inputValue}
          label={this.props.label}
          change={this.props.inputChange}
        />
      </div>
    );
  }
}

Query.propTypes = {
  selectValue: React.PropTypes.string,
  inputValue: React.PropTypes.string.isRequired,
  selectChange: React.PropTypes.func.isRequired,
  inputChange: React.PropTypes.func.isRequired,
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  options: React.PropTypes.array.isRequired,
};
