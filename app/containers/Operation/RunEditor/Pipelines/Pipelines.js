import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';

import { configurePipeline } from '../../../../reduxActions/pipelineActions';

import CancelButton from '../../../../components/buttons/CancelButton';
import Card from '../../../../components/layouts/Card';

import Pipeline from './Pipeline';

class Pipelines extends Component {

  render() {
    return (
      <div className="wrapper">
        <h2 className="section-title">Pipelines</h2>
        <Card>
          <div className="card-block">
            {_.map(this.props.pipelines, pipeline =>
              <Pipeline key={pipeline.id} pipeline={pipeline} />
            )}
            <button
              onClick={() => {
                this.props.dispatch(configurePipeline({}));
                this.props.router.push(`/${this.props.selectedProject.slug}/pipelines/add`);
              }}
            >
              Add new pipeline
            </button>
            <CancelButton />
          </div>
        </Card>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { selectedProject, pipelines } = state;

  return {
    selectedProject,
    pipelines: pipelines.pipelines,
  };
}

export default connect(mapStateToProps)(Pipelines);
