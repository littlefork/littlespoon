import Promise from 'bluebird';
import path from 'path';
import slug from 'slug';
import _ from 'lodash/fp';
import {sha256, readJson, readDir, writeJson, prettyJson} from './lib/utils';

import api from './lib/lfApi';
import conf from '../../config/default.json';

const makeId = (p, r) => `${slug(p.name)}_${sha256(r.url).substr(0, 8)}`;

// remotes
const remotePath = path.join(conf.data_path, 'remotes');

// pepper the remote with data we need
// specifically, checking the location api
const remoteIn = _.curry(r => api.check(r)
  .then(b => _.merge(r, {available: b})));

const remoteOut = _.curry(r => _.merge(r, {
  slug: r.slug || slug(r.url),
}));

// takes files of x amounts of remotes and turns them to a list
export const remotes = (p = remotePath) => readDir(p)
    .then(_.reduce((a, x) => a.concat(readJson(p, x)), []))
    .then(rs => Promise.map(rs, remoteIn()));

export const upsertRemote = (r) => {
  const rr = remoteOut(r);
  return writeJson(`${remotePath}/${rr.slug}.json`, prettyJson(rr));
};

// projects

// Mapping to and from api
const projectIn = (p, r) => _.merge(p, {
  remote: r,
  id: p.id || makeId(p, r)
});

const projectOut = (p) => _.merge(p, {
  id: p.id || makeId(p, p.remote)
});

export const projects = () =>
  Promise.reduce(
    remotes(remotePath).then(_.filter({available: true})),
    (a, r) =>
      api.get(r, 'projects')
        .then(_.map(p => projectIn(p, r)))
        .then(_.concat(a)),
    []);

export const upsert = p =>
  api.post(p.remote, 'projects', projectOut(p));

export default {
  projects, upsert, upsertRemote, remotes
};
