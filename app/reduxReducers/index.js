import _ from 'lodash';
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { uuidv4 } from 'uuid';

import { REQUEST_PROJECTS, UPDATE_PROJECT, UPDATE_PROJECTS, SELECT_PROJECT, EDIT_PROJECT, ADD_REMOTE, EDIT_REMOTE, UPDATE_REMOTES, UPSERT_PROJECT } from '../reduxActions/projectActions';
import { REQUEST_QUERIES, RECEIVE_QUERIES, CONFIGURE_QUERY, UPSERT_QUERY } from '../reduxActions/queryActions';
import { REQUEST_PLUGINS, RECEIVE_PLUGINS } from '../reduxActions/plugins';
import { REQUEST_PIPELINES, RECEIVE_PIPELINES, CONFIGURE_PIPELINE, UPSERT_PIPELINE } from '../reduxActions/pipelineActions';
import { REQUEST_RUNS, RECEIVE_RUNS } from '../reduxActions/runActions';
import { ADD_ACTION, REMOVE_ACTION, REMOVE_ALL_ACTIONS, SET_ACTIVE_RUN, REMOVE_ACTIVE_RUN } from '../reduxActions/actionActions';
import { ADD_MESSAGE, HIDE_MESSAGE } from '../reduxActions/messages';


// the name of this function is the key in the store!
const runEditor = (state = {
  activeRun: {
    id: -1,
    actions: []
  }}, action) => {
  switch (action.type) {
    case ADD_ACTION:
      _.set(state.activeRun, 'actions', _.concat(state.activeRun.actions, action.action.id));
      return _.merge({}, state);
    case REMOVE_ACTION:
      _.set(state.activeRun, 'actions', _.pull(state.activeRun.actions, action.action.id));
      return _.merge({}, state);
    case REMOVE_ALL_ACTIONS:
      _.set(state.activeRun, 'actions', []);
      return _.merge({}, state);
    case REMOVE_ACTIVE_RUN:
      _.set(state.activeRun, 'id', -1);
      return state;
    default:
      return state;
  }
};

const selectedProject = (state = {}, action) => {
  switch (action.type) {
    case SELECT_PROJECT:
      return action.payload;
    default:
      return state;
  }
};

const projects = (state = {
  configureObject: {
    name: '',
    remote: {},
    slug: null,
  }},
  action) => {
  switch (action.type) {
    case REQUEST_PROJECTS:
      return _.merge({}, state, {
        isFetching: true
      });
    case UPDATE_PROJECT:
      const pr = _.find(state.projects, {id: action.project.id});
      _.merge(pr.config, action.config);
      return state;
    case UPDATE_PROJECTS:
      return _.merge({}, state, {
        isFetching: false,
        projects: action.payload,
      });
    case EDIT_PROJECT:
      return Object.assign({}, state, {
        configureObject: action.payload || state.configureObject,
      });
    case UPSERT_PROJECT:
      _.remove(state.projects, (a) => a.id === action.payload.id);
      return Object.assign({}, state, {
        configureObject: action.payload || state.configureObject,
        // sloppy for a second:
        projects: state.projects.concat(action.payload)
      });
    case UPDATE_REMOTES:
      return Object.assign({}, state, {
        remotes: action.payload,
      });
    default:
      return state;
  }
};

const remotes = (state = {
  remotes: [],
  configureObject: {
    id: '',
    name: '',
    url: '',
    apiKey: '',
    available: false
  }},
  action) => {
  switch (action.type) {
    case ADD_REMOTE:
      return Object.assign({}, state, {
        configureObject: action.payload || state.configureObject,
        remotes: state.remotes.concat(action.payload)
      });
    case UPDATE_REMOTES:
      return Object.assign({}, state, {
        remotes: action.payload,
      });
    case EDIT_REMOTE:
      return Object.assign({}, state, {
        configureObject: action.payload || state.configureObject,
      });
    default:
      return state;
  }
};

const defaultConfQ = {
  queries: [],
  config: {},
};

const queries = (state = {
  queries: [],
  configureObject: defaultConfQ,
}, action) => {
  switch (action.type) {
    case REQUEST_QUERIES:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_QUERIES:
      return Object.assign({}, state, {
        isFetching: false,
        queries: action.payload
      });
    case CONFIGURE_QUERY:
      return Object.assign({}, state, {
        configureObject: _.merge(action.action, defaultConfQ)
      });
    case UPSERT_QUERY:
      _.remove(state.queries, (a) => a.id === action.payload.id);
      return Object.assign({}, state, {
        configureObject: action.payload || state.configureObject,
        // sloppy for a second:
        queries: state.queries.concat(action.payload)
      });
    default:
      return state;
  }
};

const plugins = (state = {}, action) => {
  switch (action.type) {
    case REQUEST_PLUGINS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_PLUGINS:
      return Object.assign({}, state, {
        isFetching: true,
        plugins: action.payload
      });
    default:
      return state;
  }
};

const defaultConfP = {
  plugins: [],
  config: {},
  pipelines: [],
};

const pipelines = (state = {
  configureObject: defaultConfP,
  pipelines: [],
}, action) => {
  switch (action.type) {
    case REQUEST_PIPELINES:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_PIPELINES:
      return Object.assign({}, state, {
        isFetching: false,
        pipelines: action.payload,
      });
    case CONFIGURE_PIPELINE:
      return Object.assign({}, state, {
        configureObject: _.merge(action.action, defaultConfP)
      });
    case UPSERT_PIPELINE:
      _.remove(state.pipelines, (a) => a.id === action.payload.id);
      return Object.assign({}, state, {
        configureObject: action.payload || state.configureObject,
        pipelines: state.pipelines.concat(action.payload)
      });
    default:
      return state;
  }
};

const runs = (state = {}, action) => {
  switch (action.type) {
    case REQUEST_RUNS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_RUNS:
      return Object.assign({}, state, {
        isFetching: true,
        runs: action.payload,
      });
    default:
      return state;
  }
};

const messages = (state = { messages: [], log: [] }, action) => {
  switch (action.type) {
    case ADD_MESSAGE:
      return Object.assign({}, state, {
        messages: state.messages.concat(action.message),
        log: state.log.concat(action.message)
      });
    case HIDE_MESSAGE:
      return Object.assign({}, state, {
        messages: _.without(state.messages, action.message),
      });
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  selectedProject,
  projects,
  remotes,
  queries,
  pipelines,
  runs,
  runEditor,
  messages,
  plugins,
  routing: routerReducer
});

export default rootReducer;
