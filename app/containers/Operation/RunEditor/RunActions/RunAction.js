
import React, { Component } from 'react';
import { connect } from 'react-redux';

import PipelineAction from './PipelineAction';
import QueryAction from './QueryAction';
import { getActionById } from '../RunEditorUtils';

class RunAction extends Component {

  render() {
    // mapping action ID to query or pipeline object
    let action = getActionById(this.props.queries, this.props.actionId);
    if (action) {
      return (<QueryAction action={action} />);
    } else {
      action = getActionById(this.props.pipelines, this.props.actionId);
      if (action) {
        return (<PipelineAction action={action} />);
      }
    }
  }
}

function mapStateToProps(state) {
  const { queries, pipelines } = state;
  return {
    queries: queries.queries,
    pipelines: pipelines.pipelines
  };
}

export default connect(mapStateToProps)(RunAction);
