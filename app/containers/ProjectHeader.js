import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router';

class ProjectHeader extends Component {
  render() {
    return (
      <div className="wrapper">
        <h1>
          <Link to={`/`}>{'<'} </Link>
          Project: {this.props.selectedProject.name}
        </h1>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { selectedProject } = state;

  return {
    selectedProject,
  };
}

export default connect(mapStateToProps)(ProjectHeader);
