import _ from 'lodash';
import React, { Component } from 'react';

import Card from '../layouts/Card';
import RunAction from '../../containers/Operation/RunEditor/RunActions/RunAction';

import styles from '../../sass/RunEditor.sass';


export default class RunEditorComponent extends Component {

  render() {
    return (
      <div>
        <h2 className="section-title">Run editor</h2>
        <button
          onClick={this.props.clear}
        >
          Clear editor
        </button>
        <form onSubmit={this.props.submit}>
          {_.map(this.props.actions, actionId => <RunAction key={actionId} actionId={actionId} />)}

          <Card>
            <h3 className={styles.cardtitle}>Add new action</h3>
            <div className="card-block">
              <a href="#" onClick={this.props.getSomething}>{'Get something'}</a>
              <span>&nbsp;or&nbsp;</span>
              <a href="#" onClick={this.props.doSomething}>{'Do something'}</a>
            </div>
          </Card>
          <button type="submit">
            {'Run'}
          </button>

        </form>
      </div>
    );
  }
}

RunEditorComponent.propTypes = {
  clear: React.PropTypes.func.isRequired,
  submit: React.PropTypes.func.isRequired,
  getSomething: React.PropTypes.func.isRequired,
  doSomething: React.PropTypes.func.isRequired,
  actions: React.PropTypes.array.isRequired,
};
