import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';

import ProjectChooser from './containers/Projects/ProjectChooser';
import EditProject from './containers/Projects/EditProject';
import AddRemote from './containers/Remotes/AddRemote';

import Operations from './containers/Operation/Operations';

import RunEditor from './containers/Operation/RunEditor/RunEditor';
import Queries from './containers/Operation/RunEditor/Queries/Queries';
import Pipelines from './containers/Operation/RunEditor/Pipelines/Pipelines';

import QueryEditor from './containers/Operation/RunEditor/Queries/QueryEditor';
import PipelineEditor from './containers/Operation/RunEditor/Pipelines/PipelineEditor';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={ProjectChooser} />
    <Route path="/edit" component={EditProject} />
    <Route path="/remote" component={AddRemote} />

    <Route path="/:project" component={Operations} >
      <IndexRoute component={RunEditor} />
      <Route path="queries" component={Queries} />
      <Route path="queries/add" component={QueryEditor} />

      <Route path="pipelines" component={Pipelines} />
      <Route path="pipelines/add" component={PipelineEditor} />
    </Route>
  </Route>
);
