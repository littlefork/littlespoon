import React, { Component } from 'react';

import Card from '../layouts/Card';
import CancelButton from '../buttons/CancelButton';
import styles from '../../sass/form.sass';

export default class ProjectEditorComponent extends Component {

  render() {
    return (
      <div>
        <div className={`${styles.heading}`}>
          <h1>Update Project {this.props.project.name}</h1>
          <small>{this.props.project.slug}</small>
        </div>
        <form onSubmit={this.props.submit} className={`${styles.tableGridWrapper} ${styles.withForm} active`}>

          {this.props.metaFields}
          <hr />
          <h5>Defaults</h5>

          {this.props.configFields}

          <hr />

          <input type="submit" value="save" />

        </form>
        <hr />
        <CancelButton />
      </div>
    );
  }
}


ProjectEditorComponent.propTypes = {
  project: React.PropTypes.object.isRequired,
  submit: React.PropTypes.func.isRequired,
  metaFields: React.PropTypes.object.isRequired,
  configFields: React.PropTypes.object,
};

ProjectEditorComponent.defautProps = {
  configFields: []
};
