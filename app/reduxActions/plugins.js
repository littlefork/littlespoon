
import {plugins} from '../src/plugins';

// PLUGIN ACTIONS

export const REQUEST_PLUGINS = 'REQUEST_PLUGINS';
export const RECEIVE_PLUGINS = 'RECEIVE_PLUGINS';
export const SELECT_PLUGIN = 'SELECT_PLUGIN';
export const CONFIGURE_PLUGIN = 'CONFIGURE_PLUGIN';

export const requestPlugins = () => ({
  type: REQUEST_PLUGINS,
});

export const receivePlugins = (json) => ({
  type: RECEIVE_PLUGINS,
  payload: json
});

export const fetchPlugins = (project) =>
  dispatch => {
    dispatch(requestPlugins);
    return plugins(project)
      .then(json => dispatch(receivePlugins(json)));
  };

export const configureQuery = (action) => ({
  type: CONFIGURE_PLUGIN,
  action
});
