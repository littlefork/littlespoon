import React, { Component } from 'react';
import { connect } from 'react-redux';

class Header extends Component {
  render() {
    return (
      <div className="header">
        Littlespoon
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { messages } = state.messages;

  return {
    messages,
  };
}

export default connect(mapStateToProps)(Header);
