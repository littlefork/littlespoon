import React, { Component } from 'react';
import { hashHistory } from 'react-router';
import { connect } from 'react-redux';
import _ from 'lodash';
import { remotesPhrases, projectPhrases } from '../../phrases';
import styles from '../../sass/remotes.sass';
import Project from '../Projects/Project';
import { editProject, editRemote } from '../../reduxActions/projectActions';


const mapStateToProps = (state) =>
({
  projects: state.projects.projects,
  projectConfigureObject: state.projects.configureObject
});

class Remote extends Component {
  constructor(props) {
    super(props);
    this.projectAdd = this.projectAdd.bind(this);
    this.remoteEdit = this.remoteEdit.bind(this);
  }

  remoteEdit = (e) => {
    const r = this.props.remote;
    console.log(this.props.remote);
    e.preventDefault();
    hashHistory.push('/remote');
    this.props.dispatch(editRemote(r));
  }

  projectAdd = (e) => {
    const p = {};
    e.preventDefault();
    hashHistory.push('/edit');
    this.props.dispatch(editProject(p));
  }

  render() {
    const projectsByRemote =
      _.filter(this.props.projects, (p) => p.remote.name === this.props.remote.name);

    const projectList =
      projectsByRemote ?
      _.map(projectsByRemote, (elem, idx) =>
        <Project
          key={idx}
          project={elem}
        />
    ) :
      <div>${remotesPhrases.noProjectsMessage}</div>;

    const showProjects =
      this.props.className === 'active' ?
        (<div className={`${styles.projects}`}>
          <div className={`${styles.span}`}>
            <h5>{_.size(projectsByRemote)} Project{(_.size(projectsByRemote) === 1 ? '' : 's')}</h5>
            <div className={`${styles.actionButtons}`}>
              <button
                className={`${styles.button} ${styles.add} btn btn-sm`}
                onClick={this.projectAdd}
              >
                <i className="fa fa-plus-circle" /> {projectPhrases.buttonAddNew}
              </button>
            </div>
          </div>
          {projectList}
        </div>) :
        '';

    return (
      <div className={`${styles.gridRow} ${this.props.className}`}>
        <div className={`${styles.span}`}>
          <i
            className={`fa fa-${this.props.icon}`}
          />
          <h4>{this.props.remote.name}</h4>
          <div className={`${styles.info}`}>{this.props.remote.url}</div>
          <div className={`${styles.actions}`}>
            <button
              className="btn btn-sm"
              onClick={this.remoteEdit}
            >
              <i className="fa fa-cogs" />
            </button>
          </div>
        </div>
        {showProjects}
      </div>
    );
  }
}

Remote.propTypes = {
  remote: React.PropTypes.object.isRequired,
  icon: React.PropTypes.string.isRequired,
  className: React.PropTypes.string.isRequired,
};

export default connect(mapStateToProps)(Remote);
