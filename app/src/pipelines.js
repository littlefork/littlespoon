import _ from 'lodash/fp';
import api from './lib/lfApi';
import {sha256} from './lib/utils';

const pipelines = (project) =>
  api.get(project.remote, `projects/${project.id}/pipelines`)
    .then(_.map(pipelineIn()));

const pipelineIn = _.curry(
  (p) => _.merge(p, {
    id: p.id || sha256(p.name)
  }));

const pipelineOut = _.curry(
  (p) => _.merge(p, {
    id: p.id || sha256(p.name),
  }));

const upsert = (p, pip) =>
  api.post(p.remote, `projects/${p.id}/pipelines`, pipelineOut(pip));

export default {
  pipelines, upsert
};
