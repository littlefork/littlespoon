import React, { Component } from 'react';
import _ from 'lodash';

import Remote from './Remote';
import Help from '../layouts/Help';
import styles from '../../sass/remotes.sass';
import { remotesPhrases } from '../../phrases';

export default class Remotes extends Component {

  render() {
    const activeRemotes = _.filter(this.props.remotes, {available: true, active: true});
    const inactiveRemotes = _.filter(this.props.remotes, {available: false, active: true});
    const disabledRemotes = _.filter(this.props.remotes, {active: false});
    return (
      <div>
        <div className={styles.heading}>
          <h1>
            <i className={`iconPage fa fa-${remotesPhrases.iconPage}`} />
            {remotesPhrases.pageTitle}
          </h1>
          <div className={`${styles.actionButtons}  pull-right`}>
            <button
              className={`${styles.button} ${styles.add} btn btn-sm`}
              onClick={this.props.newRemote}
            >
              <i className="fa fa-plus-circle" /> {remotesPhrases.buttonAddNew}
            </button>
            <Help
              dataFor="pageHelp"
              place="left"
              helpText={remotesPhrases.pageHelp}
            />
          </div>
        </div>

        <div className={`${styles.tableGridWrapper} ${styles.active}`}>
          <h3>{remotesPhrases.activeSectionTitle}
            <Help
              dataFor="activeSectionHelp"
              place="right"
              helpText={remotesPhrases.activeSectionHelp}
              buttonClass="smallHelpIcon"
            />
            <span className={`${styles.badge}`}>{_.size(activeRemotes)}</span>
          </h3>
          { _.map(activeRemotes, (elem, idx) =>
            <Remote
              key={idx}
              remote={elem}
              icon="link"
              className="active"
            />
          )}
        </div>
        <div className={`${styles.tableGridWrapper} ${styles.inactive}`}>
          <h3>{remotesPhrases.inactiveSectionTitle}
            <Help
              dataFor="inactiveSectionHelp"
              place="right"
              helpText={remotesPhrases.inactiveSectionHelp}
              buttonClass="smallHelpIcon"
            />
            <span className={`${styles.badge}`}>{_.size(inactiveRemotes)}</span>
          </h3>
          { _.map(inactiveRemotes, (elem, idx) =>
            <Remote
              key={idx}
              remote={elem}
              icon="unlink"
              className="inactive"
            />
          )}
        </div>
        <div className={`${styles.tableGridWrapper} ${styles.disabled}`}>
          <h3>{remotesPhrases.disabledRemotesTitle}
            <Help
              dataFor="disabledRemotesHelp"
              place="right"
              helpText={remotesPhrases.disabledRemotesHelp}
              buttonClass="smallHelpIcon"
            />
            <span className={`${styles.badge}`}>{_.size(disabledRemotes)}</span>
          </h3>
          { _.map(disabledRemotes, (elem, idx) =>
            <Remote
              key={idx}
              remote={elem}
              icon="ban"
              className="disabled"
            />
          )}
        </div>
      </div>
    );
  }
}


Remotes.propTypes = {
  remotes: React.PropTypes.array.isRequired,
  newRemote: React.PropTypes.func.isRequired,
};
